#include <assets.h>

//! Prepares graphical elements for use.
void assets::init(sf::RenderWindow &window, std::string cwd)
{
    if (!font.loadFromFile(cwd + fontPath))
    {
        std::cout << "Error loading font file: " << fontPath << "\n";
    }
    
    sfGuiText.setFont(font);
    sfGuiText.setCharacterSize(14);
    sfGuiText.setFillColor(sf::Color::White);
    
    sfInputText.setFont(font);
    sfInputText.setCharacterSize(14);
    sfInputText.setFillColor(sf::Color::White);
    sfInputText.setPosition(12, window.getView().getSize().y - 30);

    sfMonitorText.setFont(font);
    sfMonitorText.setCharacterSize(14);
    sfMonitorText.setFillColor(sf::Color::White);
    
    entryBar.setOutlineThickness(3);
    entryBar.setSize(sf::Vector2f(330, 24));
    entryBar.setFillColor(sf::Color(25, 25, 25, 255));
    entryBar.setOutlineColor(sf::Color(25, 40, 40, 255));
    entryBar.setPosition(10, window.getView().getSize().y - 32);
    
    menuBar.setOutlineThickness(3);
    menuBar.setFillColor(sf::Color(25, 25, 25, 255));
    menuBar.setOutlineColor(sf::Color(25, 40, 40, 255));
    menuBar.setPosition(0, window.getView().getSize().y - 40);
    
    yAxisBar.setPosition(0, 0);
    yAxisBar.setFillColor(sf::Color(0, 0, 0));
    yAxisBar.setSize(sf::Vector2f(20, window.getView().getSize().y - 30));

    guiBackground.setOutlineThickness(3);
    guiBackground.setFillColor(sf::Color(25, 25, 25, 255));
    guiBackground.setOutlineColor(sf::Color(25, 40, 40, 255));
    
    float x = (window.getView().getSize().x / 2) - 125;
    float y = (window.getView().getSize().y / 2) - 150;
    guiBackground.setSize(sf::Vector2f(300, 350));
    guiBackground.setPosition(x, y);
}