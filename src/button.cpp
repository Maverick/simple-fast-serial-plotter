#include "button.h"

button sendButton;
button typeButton;
button chanButton;
button pauseButton;
button endianButton;
button lineEndButton;
button plotterButton;

//! Buttons that have tool tips.
button* ttButtons [4] = {
    &lineEndButton,
    &typeButton,
    &endianButton,
    &chanButton
};

//! Button constructor.
button::button() 
{
    x = 0;
    y = 0;
    
    hovColor = sf::Color(55, 70, 70, 255);
    normColor = sf::Color(35, 50, 50, 255);
    
    bg.setOutlineThickness(3);    
    bg.setFillColor(normColor);
    bg.setSize(sf::Vector2f(64, 24));
    bg.setOutlineColor(sf::Color(25, 40, 40, 255));
    
    label.setFont(font);
    label.setCharacterSize(12);
    label.setFillColor(sf::Color::White);
}

//! Configures the button objects.
void configureButtons(buttonConfigParams &params)
{
    sendButton.font = params.resources.font;
    sendButton.text = "SEND";
    sendButton.x = 280;
    sendButton.y = params.window.getView().getSize().y - 32;
    
    lineEndButton.font = params.resources.font;
    lineEndButton.text = lineEndLabels[params.lineEnding];
    lineEndButton.x = params.window.getView().getSize().x - 445;
    lineEndButton.y = params.window.getView().getSize().y - 32;
    
    typeButton.font = params.resources.font;
    typeButton.text = dataTypeLabels[params.dataType];
    typeButton.x = params.window.getView().getSize().x - 370;
    typeButton.y = params.window.getView().getSize().y - 32;
    
    endianButton.font = params.resources.font;
    endianButton.text = bigEndian ? "BIG" : "LITTLE";
    endianButton.x = params.window.getView().getSize().x - 295;
    endianButton.y = params.window.getView().getSize().y - 32;
    
    chanButton.font = params.resources.font;
    chanButton.text = "VARS: " + std::to_string(params.channelCount);
    chanButton.x = params.window.getView().getSize().x - 220;
    chanButton.y = params.window.getView().getSize().y - 32;
    
    pauseButton.font = params.resources.font;
    pauseButton.text = params.paused ? "RESUME" : "PAUSE";
    pauseButton.x = params.window.getView().getSize().x - 145;
    pauseButton.y = params.window.getView().getSize().y - 32;
    
    plotterButton.font = params.resources.font;
    plotterButton.text = params.plotterActive ? "MONITOR" : "PLOTTER";
    plotterButton.x = params.window.getView().getSize().x - 70;
    plotterButton.y = params.window.getView().getSize().y - 32;
}

//! Draws a button.
void button::draw(sf::RenderWindow &window, sf::Vector2i &mousePos)
{
    bg.setPosition(x, y);
    bool hover = bg.getGlobalBounds().contains(mousePos.x, mousePos.y);
    
    for (int i = 0; i < 4; ++i)
    {
        sf::Color color = hover ? hovColor : normColor;
        bg.setFillColor(color);
    }

    int xOffset = (64 - (label.getString().getSize() * 8)) / 2;
    label.setPosition(x + xOffset, y + 3);
    label.setString(text);

    window.draw(bg);
    window.draw(label);
}