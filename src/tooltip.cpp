#include <tooltip.h>

sf::Text ttText;
bool toolTipsEnabled = true;
sf::RectangleShape ttBackground;

//! Button tool tip text.
static std::string buttonToolTips [4] = {
    "line ending",
    "data type",
    "endianness",
    "# of variables"
};

//! Gets the distance between two vectors.
float getDistance(sf::Vector2f v1, sf::Vector2f v2)
{
    float xdif = v2.x - v1.x;
    float ydif = v2.y - v1.y;
    float xdifsqrd = xdif * xdif;
    float ydifsqrd = ydif * ydif;
    return sqrt(xdifsqrd + ydifsqrd);
}

//! Updates tool tips.
void updateToolTips(sf::RenderWindow &window, assets &resources, channel channels[7], sf::Vector2i msPos)
{
    bool showToolTip = false;
    bool channelToolTip = false;
    uint8_t activeChannels = 0;
    std::string toolTipValue = "";

    for (int i = 0; i < 4; ++i)
    {
        if (ttButtons[i]->bg.getGlobalBounds().contains(msPos.x, msPos.y))
        {
            toolTipValue = buttonToolTips[i];
            showToolTip = true;
            break;
        }
    }
    
    if (!showToolTip)
    {
        for (int i = 0; i < 7; ++i)
        {
            if (channels[i].enabled)
            {
                for (int j = 0; j < channels[i].plotIndex; ++j)
                {
                    sf::Vector2f mousePos = sf::Vector2f(msPos.x, msPos.y);
                    if (getDistance(mousePos, channels[i].plot[j].position) < xSpacing / 2)
                    {
                        char valueBuf[64];
                        char minBuf[64];
                        char maxBuf[64];
                        char avgBuf[64];
                        sprintf(valueBuf, "%.2f", channels[i].rawValues[j]);
                        sprintf(minBuf, "%.2f", channels[i].chanMin());
                        sprintf(maxBuf, "%.2f", channels[i].chanMax());
                        sprintf(avgBuf, "%.2f", channels[i].chanAverage());
                        std::string value(valueBuf);
                        std::string min(minBuf);
                        std::string max(maxBuf);
                        std::string average(avgBuf);
                        toolTipValue += "Variable " + std::to_string(i + 1) + '\n';
                        toolTipValue += "Reading # " + std::to_string(j) + '\n';
                        toolTipValue += "Value: " + value + '\n';
                        toolTipValue += "Min: " + min + '\n';
                        toolTipValue += "Max: " + max + '\n';
                        toolTipValue += "Average: " + average + "\n\n";
                        showToolTip = true;
                        channelToolTip = true;
                        activeChannels++;
                        break;
                    }
                }
            }
        }
    }
    
    if (showToolTip)
    {
        ttText.setFont(resources.font);
        ttText.setCharacterSize(14);
        ttText.setFillColor(sf::Color::White);
        ttText.setString(toolTipValue);
        
        int xMidPoint = window.getView().getSize().x / 2;
        int textXoffset = msPos.x > xMidPoint ? msPos.x - 210 : msPos.x + 30;
        int bgXoffset = msPos.x > xMidPoint ? msPos.x - 220 : msPos.x + 20;
        
        int yMidPoint = window.getView().getSize().y / 2;
        int textYoffset = msPos.y > yMidPoint ? msPos.y - 110 : msPos.y + 30;
        int bgYoffset = msPos.y > yMidPoint ? msPos.y - 120 : msPos.y + 20;
        
        if (channelToolTip)
        {
            ttText.setPosition(textXoffset, textYoffset);
            ttBackground.setPosition(bgXoffset, bgYoffset);
            ttBackground.setFillColor(sf::Color(25, 25, 25, 255));
            ttBackground.setOutlineThickness(3);
            ttBackground.setOutlineColor(sf::Color(25, 76, 76, 255));
            ttBackground.setSize(sf::Vector2f(200, 120 * activeChannels));
        }
        else
        {
            ttText.setPosition(msPos.x + 10, msPos.y - 42);
            ttBackground.setPosition(msPos.x, msPos.y - 40);
            ttBackground.setFillColor(sf::Color(25, 25, 25, 255));
            ttBackground.setOutlineThickness(3);
            ttBackground.setOutlineColor(sf::Color(25, 76, 76, 255));
            ttBackground.setSize(sf::Vector2f(ttText.getString().getSize() * 11, 16));
        }

        window.draw(ttBackground);
        window.draw(ttText);
    }
}