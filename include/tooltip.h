#ifndef TOOLTIP_H
#define TOOLTIP_H

#include <math.h>
#include <assets.h>
#include <button.h>
#include <channel.h>
#include <SFML/Graphics.hpp>

extern bool toolTipsEnabled;
float getDistance(sf::Vector2f v1, sf::Vector2f v2);
void drawToolTipBackground(sf::RenderWindow &window);
void updateToolTips(sf::RenderWindow &window, assets &resources, channel channels[7], sf::Vector2i msPos);

#endif
