#ifndef KEYS_H
#define KEYS_H

#include <SFML/Graphics.hpp>

//! Key bindings for toggling variables on the serial plotter.
const sf::Keyboard::Key numKeys [7] = {
    sf::Keyboard::Num1,
    sf::Keyboard::Num2,
    sf::Keyboard::Num3,
    sf::Keyboard::Num4,
    sf::Keyboard::Num5,
    sf::Keyboard::Num6,
    sf::Keyboard::Num7
};

#endif
