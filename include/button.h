#ifndef BUTTON_H
#define BUTTON_H

#include <read.h>
#include <assets.h>
#include <SFML/Graphics.hpp>

//! Line ending button text.
const std::string lineEndLabels [4] = {
    "CR+LF",
    "CR",
    "LF",
    "NONE"
};

//! Data type button text.
const std::string dataTypeLabels [9] = {
    "ASCII",
    "INT8",
    "INT16",
    "INT32",
    "UINT8",
    "UINT16",
    "UINT32",
    "FLOAT",
    "DOUBLE"
};

//! Helper class for creating buttons with SFML.
class button 
{
    public:
        button();
        int x;
        int y;
        sf::Font font;
        sf::Text label;
        sf::Color hovColor;
        sf::Color normColor;
        sf::RectangleShape bg;
        std::string text;
        void draw(sf::RenderWindow &window, sf::Vector2i &mousePos); 
};

extern button chanButton;
extern button sendButton;
extern button typeButton;
extern button pauseButton;
extern button endianButton;
extern button lineEndButton;
extern button plotterButton;
extern button* ttButtons [4];

typedef struct {
   sf::RenderWindow &window;
   assets &resources;
   uint8_t channelCount;
   uint8_t lineEnding;
   uint8_t dataType;
   bool plotterActive;
   bool paused;
} buttonConfigParams;

void configureButtons(buttonConfigParams &params);

#endif

