#ifndef PORTS_H
#define PORTS_H

#include <stdio.h>
#include <dirent.h>

//! Serial port names for GNU/Linux systems.
const char* linuxPorts [12] = {
    "/dev/ttyACM0",
    "/dev/ttyACM1",
    "/dev/ttyACM2",
    "/dev/ttyACM3",
    "/dev/ttyUSB0",
    "/dev/ttyUSB1",
    "/dev/ttyUSB2",
    "/dev/ttyUSB3",
    "/dev/ttyS0",
    "/dev/ttyS1",
    "/dev/ttyS2",
    "/dev/ttyS3",
};

//! Serial port names for Windows.
const char* winPorts [12] = {
    "\\\\.\\COM1",
    "\\\\.\\COM2",
    "\\\\.\\COM3",
    "\\\\.\\COM4",
    "\\\\.\\COM5",
    "\\\\.\\COM6",
    "\\\\.\\COM7",
    "\\\\.\\COM8",
    "\\\\.\\COM9",
    "\\\\.\\COM10",
    "\\\\.\\COM11",
    "\\\\.\\COM12"
};

//! Serial port names for Mac.
const char* macPorts [12];

//! Gets OSX port names from directory.
void getMacPorts()
{
    struct dirent *de;
    std::vector<const char*> ports;
  
    DIR *dr = opendir("/dev");
  
    if (dr == NULL)
    {
        return;
    }
  
    while ((de = readdir(dr)) != NULL)
    {
        std::string d_name(de->d_name);
        if (d_name.find("tty.usb") != std::string::npos && ports.size() < 12)
        {
            ports.push_back(de->d_name);
        }
    }
  
    closedir(dr);  
    
    for (int i = 0; i < 12; ++i) 
    {
        macPorts[i] = ports[i];
    }
}

#endif

