#ifndef CHANNEL_H
#define CHANNEL_H

#include <limits>
#include <cstddef>
#include <SFML/Graphics.hpp>

extern float xSpacing;
extern float ySpacing;
extern float plotBase;
extern float defaultPlotBase;

//! Represents an individual data point on the serial plotter.
class channel 
{
    public:
        channel();
        bool active;
        bool enabled;
        int plotIndex;
        int plotOrigin;
        sf::Color color;
        sf::VertexArray plot;
        std::vector<float>rawValues;
        float chanMin();
        float chanMax();
        float chanAverage();
        void redrawPlot();
        void drawPlot(sf::RenderWindow &window, sf::Vector2i mousePosition, std::string buffer, int plotMultiplier);
};

#endif

