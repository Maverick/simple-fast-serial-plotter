#ifndef ASSETS_H
#define ASSETS_H

#include <iostream>
#include <SFML/Graphics.hpp>

//! Colors for plotting each variable.
const sf::Color channelColors [7] = {
    sf::Color::Red,
    sf::Color::Blue,
    sf::Color::Yellow,
    sf::Color::Green,
    sf::Color::Magenta,
    sf::Color::White,
    sf::Color::Cyan
};

//! Information displayed in help window.
const std::string guiText [2] = { 
    "Up: scroll up\n"
    "Down: scroll down\n"
    "Mouse Wheel: vertical scroll\n"
    "Enter: send\n"
    "Tab: focus input field\n"
    "Backspace: delete\n"
    "F4: toggle terminal output\n"
    "F5: change line ending\n"
    "F6: change data type\n"
    "F7: change # of variables\n"
    "F8: change endianess\n"
    "F10: open monitor/plotter\n"
    "Pause/Break: pause\n",
    
    "1-7: toggle variables\n"
    "Tab: focus input field\n"
    "Mouse Wheel: vertical scroll\n"
    "L-Shift + Mouse Wheel: scale\n"
    "L-Ctrl + Mouse Wheel: y axis spacing\n"
    "Q: decrease y axis scale\n"
    "E: increase y axis scale\n"
    "W: increase y axis spacing\n"
    "S: decrease y axis spacing\n"
    "A: decrease x axis spacing\n"
    "D: increase x axis spacing\n"
    "Up: scroll up\n"
    "Down: scroll down\n"
    "Left: scroll left\n"
    "Right: scroll right\n"
    "F2: show active variables\n"
    "F3: toggle tool tips\n"
    "F4: toggle terminal output\n"
    "F5: change line ending\n"
    "F6: change data type\n"
    "F7: change # of variables\n"
    "F8: change endianness\n"
    "F10: open monitor/plotter\n"
    "Pause/Break: pause\n"
};

//! Graphic assets.
class assets
{
    public:
        sf::Font font;
        sf::Text sfGuiText;
        sf::Text sfInputText;
        sf::Text sfMonitorText;
        sf::String terminalDisplay;
        sf::String userInputDisplay;
        sf::RectangleShape entryBar;
        sf::RectangleShape menuBar;
        sf::RectangleShape yAxisBar;
        sf::RectangleShape guiBackground;
        void init(sf::RenderWindow &window, std::string cwd);
  
    private:
        const std::string fontPath = "/assets/fonts/Inconsolata/fonts/ttf/Inconsolata-ExpandedRegular.ttf";
};

#endif

