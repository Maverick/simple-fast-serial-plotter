#ifndef READ_H
#define READ_H

#include <serialib.h>
#include <arpa/inet.h>

extern bool bigEndian;

std::string readInt8(serialib &serial);
std::string readInt16(serialib &serial);
std::string readInt32(serialib &serial);
std::string readUint8(serialib &serial);
std::string readUint16(serialib &serial);
std::string readUint32(serialib &serial);
std::string readFloat(serialib &serial);
std::string readDouble(serialib &serial);

#endif