Simple Fast Serial Plotter
===============

_A serial plotter and monitor created with C++ and SFML_ <br />

<img src="https://i.imgur.com/Nfs4RI1.png" alt="SFSP" width="640" height="360">

**Features** <br />

Serial plotter capable of plotting up to 7 variables simultaneously.<br />
Automatic calculation of minimum, maximum and average values for each variable.<br />
Ability to scroll through the serial plotter's 100,000 most recent values.<br />
Manual control of scale and range allows for zooming in on any part of the graph.<br />
Binary data formats (u)int8, (u)int16, (u)int32, float, double.<br />
Sending of ASCII commands with configurable line endings.<br />
Serial monitor with automatic logging to text file of 50,000 most recent lines.<br />

**Usage** <br />

Run the SFSP executable to start the program.<br />
To specify a serial port, use the command line argument '-port' or '-p'.<br />
To specify a baud rate use the command line argument '-baud' or '-b'.<br />
For example: "./SFSP -p /dev/ttyUSB0 -b 115200".<br />
If no serial port is specified, the program will attempt to find the port used by your device.<br /><br />
Range, scale and spacing are adjusted using the keyboard and mouse.<br />
Once the program is running, press F1 to see a list of all keyboard and mouse functions.<br />
Functions will be specific to the device that is open, either the monitor or plotter.<br />

**Compiling** <br />

_Linux:_ <br />

Clone this repository. <br />
Install SFML, ie: 'sudo apt-get install libsfml-dev' <br />
Run the compile_linux.sh script <br />

_Mac:_ <br />

Clone this repository. <br />
Install g++ as described here https://www3.cs.stonybrook.edu/~alee/g++/g++_mac.html<br />
Download SFML here https://www.sfml-dev.org/download/sfml/2.5.1/ <br />
Copy the contents of the SFML Frameworks and extlibs directories to /Library/Frameworks <br />
Run the compile_mac.sh script <br />

_Windows:_ <br />

Download SFML and GCC here https://www.sfml-dev.org/download/sfml/2.5.1/ <br />
You will need 'MinGW Builds 7.3.0 (64-bit)' and SFML 'GCC 7.3.0 MinGW (SEH) - 64-bit' <br />
For 32 bit systems you will need 'MinGW Builds 7.3.0 (32-bit)' and 'GCC 7.3.0 MinGW (DW2) - 32-bit' <br />

Clone this repository to C:\ so the source code is present at C:\SFSP <br />
Extract the downloaded SFML package to C:\ so SFML is present at C:\SFML-2.5.1 <br />
Run the compile_windows.bat script <br />
