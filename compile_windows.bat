g++.exe -Wall -O2 -IC:\SFML-2.5.1\include -Iinclude -Isrc -c C:\SFSP\src\main.cpp -o build\main.o
g++.exe -Wall -O2 -IC:\SFML-2.5.1\include -Iinclude -Isrc -c C:\SFSP\src\assets.cpp -o build\assets.o
g++.exe -Wall -O2 -IC:\SFML-2.5.1\include -Iinclude -Isrc -c C:\SFSP\src\button.cpp -o build\button.o
g++.exe -Wall -O2 -IC:\SFML-2.5.1\include -Iinclude -Isrc -c C:\SFSP\src\channel.cpp -o build\channel.o
g++.exe -Wall -O2 -IC:\SFML-2.5.1\include -Iinclude -Isrc -c C:\SFSP\src\serialib.cpp -o build\serialib.o
g++.exe -Wall -O2 -IC:\SFML-2.5.1\include -Iinclude -Isrc -c C:\SFSP\src\tooltip.cpp -o build\tooltip.o
g++.exe -LC:\SFML-2.5.1\lib -o SFSP.exe build\main.o build\assets.o build\button.o build\channel.o build\serialib.o build\tooltip.o -s -v -lstdc++fs -lmingw32 -luser32 -lgdi32 -lwinmm -ldxguid -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system -mwindows
